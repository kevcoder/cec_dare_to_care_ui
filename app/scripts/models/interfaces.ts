interface ICustomerSearch {
    firstName: string,
    lastName: string,
    dob: Date
}
interface ICustomer {
    id?: number,
    firstName: string,
    lastName: string,
    primaryCustomerID?: number,
    relationshipType?: string,
    dob: Date,
    addrLine1?: string,
    addrLine2?: string,
    city?: string,
    state?: string,
    zip?: string
}
interface IServiceDate {
    id: number,
    dateTime: Date,
    checkOutTm: Date,
    canceled: boolean,
    items: string[],
    createDt: Date,
    creator: string
}
class AjaxUrls {
    static getCustomerUrl: string = 'http://192.168.0.8/api/customer/';
    static addCustomerUrl: string = 'http://192.168.0.8/api/customer/';
}
class DomManipulations {
    static modal: string = "#mdl";
    static addModalBody: string = "";
    static customerPropertyPrefix: string = ".cust-";
    static customersEditorSel: string = ".cust-editor";
    static templateSel: string = '#tmplCustomer';
    static customerTable: string = 'div.cust-data > table';
    static btnAddCustomer:string = 'button#customer-add';
    static tmpAddCustomerBody:string = 'script#tmpAddCustomerBody';
    static tmpEditCustomer:string = 'script#tmplEditCustomer';
    static notificationArea:string = '.notification-area';
}
enum NotificationTypes {
    info,
    success,
    warning,
    danger
}