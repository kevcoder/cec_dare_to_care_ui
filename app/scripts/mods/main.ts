
class Main {
    constructor() {
        let me = this;

    }

  static  DisplayNotification(message: string, messageType: NotificationTypes, parentSelector?: string) {
        parentSelector = parentSelector ||'.container ' + DomManipulations.notificationArea;
        let bsAlertClassName = 'alert-' + NotificationTypes[messageType];
        let parent = $(parentSelector);
        let alert = $(`<div class="alert alert-dismissible fade show ${bsAlertClassName}" role="alert">`
            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            + '<span aria-hidden="true">&times;</span>'
            + '</button>'
            + `<strong>!</strong> ${message}`
            + '</div>');
        
        parent.show().slideDown();
        parent.prepend(alert);



    }
}
new Main();