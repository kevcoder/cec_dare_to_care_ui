
/**
 * CustomerBL
 */
class CustomerBL {
    Customer: ICustomer;
    Customers: ICustomer[];



    Dt: any;

    constructor() {
        let me = this;
        me.GetCustomer(null);
        $(DomManipulations.btnAddCustomer).off().click(me.AddBtn_OnClick);
    }

    AddBtn_OnClick(evt: JQueryEventObject) {
        const me = this;
        let mdl = $(DomManipulations.modal);
        //copy template into modal body
        mdl.find('.modal-body').html($(DomManipulations.tmpAddCustomerBody).html());
        //set title
        mdl.find('.modal-title').html('Add A New Person');

        mdl.find('#mdl-action').off('click').click(function (evt) {
            let cust: ICustomer = {
                firstName: mdl.find(DomManipulations.customerPropertyPrefix + 'first-name').val(),
                lastName: mdl.find(DomManipulations.customerPropertyPrefix + 'last-name').val(),
                dob: mdl.find(DomManipulations.customerPropertyPrefix + 'dob').val(),
                zip: mdl.find(DomManipulations.customerPropertyPrefix + 'zip').val(),
                primaryCustomerID: mdl.find(DomManipulations.customerPropertyPrefix + 'primary-customer').val()[0]
            }

            me.SaveCustomer(cust, function (response) {
                Main.DisplayNotification("Successfull: created new user " + cust.firstName + " " + cust.lastName, NotificationTypes.success, null);
                $(DomManipulations.modal).modal('hide');
                //refresh the list of customers
                me.GetCustomer(null);
            }, null);
        });


        mdl.find('.search').off().keyup(function () {

            const customers = me.ProgressiveSearch($(this).val());
            const ddl = mdl.find(DomManipulations.customerPropertyPrefix + 'primary-customer');
            let opts = null;
            if (customers) {
                opts = $.map(customers, function (cust, idx) {
                    return `<option value="${cust.id}">${cust.firstName} ${cust.lastName}</option>`;
                }).join('');
            }
            ddl.html(opts)
        });

        mdl.modal({
            backdrop: 'static',
            keyboard: false,
            show: false,

        });
        mdl.on('hidden.bs.modal', function () {
            //refresh the grid of customers
            me.GetCustomer(null);
        });


        mdl.modal('show');

    }

    PopulateTable(customers: ICustomer[]) {

        const me = this;
        if (!this.Dt) {
            this.Dt = $(DomManipulations.customerTable).DataTable({
                paging: true,
                columns: [
                    {
                        data: null,
                        orderable: false,
                        className: 'cust-details ',
                        defaultContent: ''
                    }
                    , { data: "firstName" }
                    , { data: "lastName" }
                    , {
                        data: "dob",
                        render: function (data, type, fullRow, meta) {
                            if (type === 'display') {
                                const d = moment(data);
                                if (d.isValid())
                                    return d.format('MM/DD/YYYY');
                                else
                                    return '';
                            }
                            else
                                return data;
                        }
                    }
                    , { data: "addrLine1" }
                    , { data: "zip" }
                ]
            });
        }

        this.Dt.clear();
        this.Dt.rows.add(customers).draw();

        //register events
        me.Dt.on('dblclick', 'tr', function (evt, dt, type, idx) {
            const data = me.Dt.row(this).data();
            console.info('dt selection', data);
            me.CustomerRow_OnClick(data);
        });
    }

    PopulateEditor(customer: ICustomer) {
        const me = this;
        let mdl = $(DomManipulations.modal);
        //set title
        mdl.find('.modal-title').html('Edit Person');
        //copy template into modal body
        mdl.find('.modal-body').html($(DomManipulations.tmpEditCustomer).html());

        mdl.find(DomManipulations.customerPropertyPrefix + 'first-name').val(customer.firstName);
        mdl.find(DomManipulations.customerPropertyPrefix + 'last-name').val(customer.lastName);
        mdl.find(DomManipulations.customerPropertyPrefix + 'dob').val(moment(customer.dob).format('YYYY-MM-DD'));
        mdl.find(DomManipulations.customerPropertyPrefix + 'addr').val(customer.addrLine1);
        mdl.find(DomManipulations.customerPropertyPrefix + 'city').val(customer.city);
        mdl.find(DomManipulations.customerPropertyPrefix + 'state').val(customer.state);
        mdl.find(DomManipulations.customerPropertyPrefix + 'zip').val(customer.zip);

        mdl.modal({
            backdrop: 'static',
            keyboard: false,
            show: false,

        });

        mdl.find('#mdl-action').off('click').click(function (evt) {
            let dob = mdl.find(DomManipulations.customerPropertyPrefix + 'dob').val();
            let cust: ICustomer = {
                id: customer.id,
                firstName: mdl.find(DomManipulations.customerPropertyPrefix + 'first-name').val(),
                lastName: mdl.find(DomManipulations.customerPropertyPrefix + 'last-name').val(),
                dob: moment(dob).isValid() ? moment(dob).toJSON() : null,
                zip: mdl.find(DomManipulations.customerPropertyPrefix + 'zip').val(),
                primaryCustomerID: customer.primaryCustomerID,
                addrLine1: mdl.find(DomManipulations.customerPropertyPrefix + 'addr').val(),
                city: mdl.find(DomManipulations.customerPropertyPrefix + 'city').val(),
                state: mdl.find(DomManipulations.customerPropertyPrefix + 'state').val()
            }

            me.SaveCustomer(cust, function (response) {
                Main.DisplayNotification(`Successfull: saved ${cust.firstName} ${cust.lastName}`, NotificationTypes.success, null);
                $(DomManipulations.modal).modal('hide');
                //refresh the list of customers
                me.GetCustomer(null);
            }, null);
        });

        mdl.modal('show');

    }

    GetCustomer(id: number) {
        let url = AjaxUrls.getCustomerUrl + (id || '');
        let me = this;
        $.getJSON(url, function (cust) {
            me.Customers = cust;
            me.PopulateTable(me.Customers);
        })
            .fail(function (err) {
                console.error('failed calling ' + url);
                Main.DisplayNotification('Failed Retrieving Customers', NotificationTypes.danger)
            })
    }

    SaveCustomer(entity: ICustomer, fnDoneClbk: (response: any) => void, fnFailClbk: (err: any) => void) {
        let me = this;
        let url = AjaxUrls.addCustomerUrl;
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'json',
            data: entity
        })
            .done(function (response) {
                if (fnDoneClbk && typeof fnDoneClbk === 'function') { fnDoneClbk(response); }
            })
            .fail(function (err) {
                if (fnFailClbk && typeof fnFailClbk === 'function') { fnFailClbk(err); }
            })
    }

    ProgressiveSearch(input?: string) {
        let me = this;
        if (input == null || input.length < 3)
            return [];
        else {
            let searchWrds = input.split(' ');
            let matchedCustomers = [];
            $.each(searchWrds, function (idx, wrd) {

                let matchFirst = me.Customers.filter(function (cust, idx) {
                    return cust.firstName && cust.firstName.toLowerCase().indexOf(wrd) > -1;
                });
                let matchLast = me.Customers.filter(function (cust, idx) {
                    return cust.lastName && cust.lastName.toLowerCase().indexOf(wrd) > -1;
                });
                let matchAddr = me.Customers.filter(function (cust, idx) {
                    return cust.addrLine1 && cust.addrLine1.toLowerCase().indexOf(wrd) > -1;
                });

                $.each(matchAddr, function (idx, m) {
                    if ($.inArray(m, matchedCustomers) == -1)
                        matchedCustomers.push(m);
                })
                $.each(matchLast, function (idx, m) {
                    if ($.inArray(m, matchedCustomers) == -1)
                        matchedCustomers.push(m);
                })
                $.each(matchFirst, function (idx, m) {
                    if ($.inArray(m, matchedCustomers) == -1)
                        matchedCustomers.push(m);
                })

            });
            return matchedCustomers;
        }
    }

    CustomerRow_OnClick(entity: ICustomer) {
        const me = this;
        me.PopulateEditor(entity)

    }
}


new CustomerBL();